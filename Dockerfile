FROM python:3.8-slim-buster

WORKDIR .

COPY src .

CMD ["python3", "main.py"]