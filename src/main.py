import json
import random
from pprint import pprint
from datetime import datetime, timedelta
from typing import List, TypedDict, Tuple


class Stock(TypedDict):
    date: str
    price: float


def find_max_profit_with_one_transaction(stocks: List[Stock]) -> Tuple[str, str, float]:
    current_min_stock = stocks[0]
    max_profit = 0
    min_stock_date = ""
    max_stock_date = ""
    for stock in stocks:
        profit = stock["price"] - current_min_stock["price"]
        if profit > max_profit:
            max_profit = profit
            min_stock_date = current_min_stock["date"]
            max_stock_date = stock["date"]
        if stock["price"] < current_min_stock["price"]:
            current_min_stock = stock
    return min_stock_date, max_stock_date, max_profit


def find_max_profit_with_many_transactions(
    stocks: List[Stock],
) -> Tuple[List[Tuple[str, str, float]], float]:
    results = []
    max_profit = 0
    current_profit = 0
    buy_stock = None
    for i in range(len(stocks) - 1):
        difference = stocks[i + 1]["price"] - stocks[i]["price"]
        if difference >= 0:
            max_profit += difference
            current_profit += difference
            if buy_stock is None:
                buy_stock = stocks[i]
            if i + 1 == len(stocks) - 1:
                results.append((buy_stock["date"], stocks[i + 1]["date"], current_profit))
        else:
            if buy_stock is not None:
                results.append((buy_stock["date"], stocks[i]["date"], current_profit))
                current_profit = 0
                buy_stock = None
    return results, max_profit


def generate_stock_prices(
    start_date: datetime = datetime(year=2021, month=1, day=1), number_of_days: int = 10
) -> List[Stock]:
    return [
        {
            "date": (start_date + timedelta(days=day)).strftime("%Y-%m-%d"),
            "price": random.randrange(0, 100, 1),
        }
        for day in range(number_of_days)
    ]


if __name__ == "__main__":
    stock_prices = generate_stock_prices()
    print('List of stock prices')
    pprint(stock_prices, indent=2)
    print()

    (buy_date1, sell_date1, max_profit) = find_max_profit_with_one_transaction(stock_prices)
    print('with one transaction')
    print(f'\tbuy on {buy_date1} and sell on {sell_date1} with max profit {max_profit}')
    print()

    (transactions, total_profit) = find_max_profit_with_many_transactions(stock_prices)
    print(f'with many transactions, with total profit {total_profit}')
    for (buy_date2, sell_date2, profit) in transactions:
        print(f'\tbuy on {buy_date2} and sell on {sell_date2} to get {profit}')
